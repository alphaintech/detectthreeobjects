#include <stdio.h>
#include <iostream>
#include "opencv/cv.h"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;

//��������� ����������� � ������������ � gray
Mat convertToGray(Mat image);

//��������� ������� �����������
vector<vector<Point> > getContours(Mat image);

//���������� ������� � ��������
vector<vector<Point> > checkContours(Mat src, Mat templ);

//�������� � ������
void checkColor(Mat src, Mat templ);