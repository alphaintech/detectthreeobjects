// DetectThreeObjects.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include "contours.h"

using namespace cv;
using namespace std;

void findObjectsOnImage(String src_name, String templ_name)
{
	//�� ��������� ���������
	Mat src = imread(src_name, CV_LOAD_IMAGE_COLOR);
	Mat templ = imread(templ_name, CV_LOAD_IMAGE_COLOR);

	//imshow("Source", src);
	//imshow("Template", templ);
	checkColor(src, templ);

	String srcFindColor_name = "../data/find.jpg";
	Mat srcFindColor = imread(srcFindColor_name, CV_LOAD_IMAGE_COLOR);

	vector<vector<Point> > match_contour = checkContours(srcFindColor, templ);

	if (!match_contour.empty())
	{
		for (int i = 0; i < match_contour.size(); i++)
		{
			Scalar color = Scalar(0, 255, 0);
			drawContours(src, match_contour, i, color, 2, 8, 0, 0, Point());
		}
		imshow("Match Contours", src);
	}
}

void findObjectsOnVideo(String templ_name)
{
	//�� 30-�� ����� �� �����
	Mat frame;
	VideoCapture cap;
	int camOpen = cap.open(CV_CAP_ANY);

	namedWindow("window", CV_WINDOW_AUTOSIZE);

	Mat templ = imread(templ_name, CV_LOAD_IMAGE_COLOR);
	int i = 0;

	while (true) {
		cap >> frame;
		imshow("window", frame);

		// delay 33ms
		waitKey(33);
		if (i == 30) {
			checkContours(frame, templ);
			i = 0;
		}
		i++;
	}
}

void detectAndDisplay(Mat frame)
{
	String cascade_file = "cascade.xml";
	CascadeClassifier cascade;
	if (!cascade.load(cascade_file))
	{
		printf("--(!)Error loading cascade\n");
		waitKey(0);
	};

	vector<Rect> faces;
	Mat frame_gray;
	cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);
	//-- Detect faces
	cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
	for (size_t i = 0; i < faces.size(); i++)
	{
		Point center(faces[i].x + faces[i].width / 2, faces[i].y + faces[i].height / 2);
		ellipse(frame, center, Size(faces[i].width / 2, faces[i].height / 2), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
		Mat faceROI = frame_gray(faces[i]);		
	}
	//-- Show what you got
	imshow("Detection", frame);
}

int _tmain(int argc, _TCHAR* argv[])
{
	//�� 30-�� ����� �� �����
	//String templ_name = "../data/rectangle.jpg";
	//findObjectsOnVideo(templ_name);
	
	
	//�� ��������� ���������
	String src_name = "../data/0.bmp";
	//String templ_name = "../data/redcircle.jpg";
	//findObjectsOnImage(src_name, templ_name);
	Mat src = imread(src_name, CV_LOAD_IMAGE_COLOR);

	detectAndDisplay(src);

	waitKey(0);
	return(0);
}



/* @function detectAndDisplay */

