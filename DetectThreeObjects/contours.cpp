#include "stdafx.h"
#include "contours.h"

Mat convertToGray(Mat image)
{
	cvtColor(image, image, CV_RGB2GRAY);
	blur(image, image, Size(3, 3));
	return image;
}

vector<vector<Point> > getContours(Mat image)
{
	Mat canny_output;
	vector<vector<Point> > contours;
	int thresh = 150;

	// Detect edges using canny
	Canny(image, canny_output, thresh, thresh * 2, 3);
	// Find contours
	findContours(canny_output, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	return contours;
}

vector<vector<Point> > checkContours(Mat src, Mat templ)
{
	vector<vector<Point> > src_contour = getContours(convertToGray(src));
	vector<vector<Point> > templ_contour = getContours(convertToGray(templ));

	//������ ������� �����
	Mat img_scene = Mat::zeros(src.size(), CV_8UC3);
	for (int i = 0; i < src_contour.size(); i++)
	{
		Scalar color = Scalar(255, 255, 255);
		drawContours(img_scene, src_contour, i, color, 2, 8, 0, 0, Point());
	}
	imshow("Source contours", img_scene);

	//������ ������� �������
	/*Mat img_object = Mat::zeros(templ.size(), CV_8UC3);
	for (int i = 0; i < templ_contour.size(); i++)
	{
	Scalar color = Scalar(255, 255, 255);
	drawContours(img_object, templ_contour, i, color, 2, 8, 0, 0, Point());
	}
	imshow("Template contours", img_object);*/

	// ������� ������� ����������� 
	int counter = 0;
	double matchM = 1000;
	vector<vector<Point> > match_contour;

	if (!(src_contour.empty() || templ_contour.empty()))
	{
		for (vector<vector<Point> >::iterator it = src_contour.begin(); it != src_contour.end(); ++it){
			double match0 = matchShapes(*it, templ_contour[0], CV_CONTOURS_MATCH_I1, 0);
			if (match0 < matchM){
				matchM = match0;
				match_contour.push_back(*it);
			}
		}

		/*for (int i = 0; i < match_contour.size(); i++)
		{
		Scalar color = Scalar(0, 255, 0);
		drawContours(src, match_contour, i, color, 2, 8, 0, 0, Point());
		}*/
	}

	//imshow("Match Contours", src);
	return match_contour;
}

void checkColor(Mat src, Mat templ)
{
	int Rmin;
	int Rmax;
	int Gmin;
	int Gmax;
	int Bmin;
	int Bmax;

	//------������ � ��������-------

	// ��� �������� ������� RGB
	Mat rgb_templ;
	Mat channels[3];

	//  ��������
	templ.copyTo(rgb_templ);

	// ��������� �� ��������� ������
	split(rgb_templ, channels);

	double framemin;
	double framemax;

	// �������� ���� ������� (��������������� ��� �� ����)
	minMaxLoc(channels[0], &framemin, &framemax);
	Rmin = framemin;
	Rmax = framemax;
	minMaxLoc(channels[1], &framemin, &framemax);
	Gmin = framemin;
	Gmax = framemax;
	minMaxLoc(channels[2], &framemin, &framemax);
	Bmin = framemin;
	Bmax = framemax;

	//------������ � ���������-------
	Mat rgb_src;
	blur(src, rgb_src, Size(20, 20));
	rgb_src.copyTo(rgb_src);
	split(rgb_src, channels);

	Mat channels_range[3];

	// ��� �������� ��������� ��������
	Mat rgb_and;

	inRange(channels[0], Scalar(Rmin), Scalar(Rmax), channels_range[0]);
	inRange(channels[1], Scalar(Gmin), Scalar(Gmax), channels_range[1]);
	inRange(channels[2], Scalar(Bmin), Scalar(Bmax), channels_range[2]);
	imshow("R", channels[0]);
	imshow("G", channels[1]);
	imshow("B", channels[2]);

	bitwise_and(channels_range[0], channels_range[1], rgb_and);
	bitwise_and(rgb_and, channels_range[2], rgb_and);
	imshow("RGB", rgb_and);

	imwrite("../data/find.jpg", rgb_and);
}